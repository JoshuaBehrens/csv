#ifndef _CSV_HPP
	#define _CSV_HPP

	#include <iostream>
	#include <sstream>
	#include <fstream>
	#include <exception>
	#include <list>

template <typename CHART> class CSV
{
public:
	typedef CHART Character;
	typedef std::basic_string<Character> String;
	typedef std::list<String> Row;
	typedef std::list<Row> Table;
	
	CSVParser(Character row, Character col, Character text, Character esc) : _rowdel(row), _coldel(col), _textdel(text), _escape(esc)
	{
	}
	
	Table Read(std::string file)
	{
		std::fstream str(file.c_str(), std::fstream::in);
		return Read(str);
	}
	
	Table Read(std::istream& stream)
	{
		if (stream)
			return _readTable(stream);
		
		throw std::runtime_error("std::istream invalid.");
	}
	
	void Write(std::string file, Table t)
	{
		std::ofstream str(file.c_str(), std::ofstream::trunc);
		Write(str, t);
	}
	
	void Write(std::ofstream& stream, Table t)
	{
		_writeTable(stream, t);
	}
private:
	Character _rowdel = 0;
	Character _coldel = 0;
	Character _textdel = 0;
	Character _escape = 0;
	Character _currentChar = 0;
	
	bool _pullChar(std::istream& str)
	{
		Character result;
		
		if (!str.eof() && str.read((std::istream::char_type*)&result, sizeof(Character)))
		{
			_currentChar = result;
			return true;
		}
		
		return false;
	}
	
	bool _pushChar(std::ostream& str, Character c)
	{
		return str.write((std::ostream::char_type*)&c, sizeof(Character));
	}
	
	Table _readTable(std::istream& str)
	{
		Table result;
		
		while (true)
		{
			result.push_back(_readRow(str));
			if (str.eof())
				break;
			if (_currentChar != _rowdel)
				throw std::runtime_error("expected end of stream or row delimiter");
		}
		
		return result;
	}

	void _writeTable(std::ostream& str, Table t)
	{
		bool firstRow = true;
		
		for (typename Table::iterator r(t.begin()); r != t.end(); ++r)
		{
			if (!firstRow)
				_pushChar(str, _rowdel);
				
			_writeRow(str, *r);
			
			firstRow = false;
		}
	}

	Row _readRow(std::istream& str)
	{
		Row result;
		
		while (true)
		{
			result.push_back(_readItem(str));
			if (str.eof() || _currentChar == _rowdel)
				break;
			if (_currentChar != _coldel)
				throw std::runtime_error("expected end of stream or row delimiter or column delimiter");
		}
		
		return result;
	}

	void _writeRow(std::ostream& str, Row r)
	{
		bool firstCol = true;
		
		for (typename Row::iterator c(r.begin()); c != r.end(); ++c)
		{
			if (!firstCol)
				_pushChar(str, _coldel);
				
			_writeItem(str, *c);
			
			firstCol = false;
		}
	}
	
	String _readItem(std::istream& str)
	{
		std::basic_ostringstream<Character> result;
	
		if (_pullChar(str))
		{
			if (_currentChar == _textdel)
			{
				if (!_pullChar(str))
					return result.str();

				bool dotest = true;
				do
				{
					if (dotest)
					{
						if (_currentChar == _textdel)
						{
							_pullChar(str);
							break;
						}
						else if (_currentChar == _escape)
						{
							dotest = false;
							continue;
						}
					}
					dotest = true;
					result << _currentChar;
				}
				while (_pullChar(str));
			}
			else
				do
					result << _currentChar;
				while (_pullChar(str) && _currentChar != _coldel);

		}
		return result.str();
	}

	void _writeItem(std::ostream& str, String s)
	{
		if (s.find(_textdel) == String::npos)
			for (typename String::iterator c(s.begin()); c != s.end(); ++c)
				_pushChar(str, *c);
		else
		{
			_pushChar(str, _textdel);
			for (typename String::iterator c(s.begin()); c != s.end(); ++c)
			{
				if (*c == _textdel || *c == _escape)
					_pushChar(str, _escape);
				_pushChar(str, *c);
			}
			_pushChar(str, _textdel);
		}
	}
};

#endif // _CSV_HPP

