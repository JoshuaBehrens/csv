#include "CSV.hpp"

int main(int argc, char **argv)
{
	try
	{
		CSV<char> parser('\n', ',', '"', '\\');
		if (argc > 1)
		{
			CSV<char>::Table tab = parser.Read(std::string(argv[1]));
			parser.Write(std::string(argv[1]), tab);
		}
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		std::cin.get();
	}
	
	return 0;
}

