##CSV Parser

This is a quite simple but flexible class  to read and write csv files and streams. The c++ code is header only and does only depend on standard headers like fstream or string.

The code is public domain.

##Contribute
You are free to add your own portations for different languages, implementations for different environments and improvements to existing code. By adding your files to this repository you agree to publishing your code on public domain to keep the lincensing simple. BUT your name will not vanish. I list all the contributers here.
Your code should be setup like the rest of the code already posted (this can be ignored if a language is not supporting a certain feature like the others e.g. classes). This keeps the documentation simple as well.

##Contributers

- [Joshua Behrens](http://www.joshua-behrens.de/) (c++)

